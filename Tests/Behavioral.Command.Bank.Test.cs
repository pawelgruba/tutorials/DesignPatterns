﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesignPatterns.Behavioral.Command.BankAccount;

namespace Tests
{
	[TestClass]
	public class BehavioralCommandBankTest
	{
		[TestMethod]
		public void AllTransactionsSuccessful()
		{
			TransactionManager transactionManager = new TransactionManager();

			Account pawel = new Account("Pawel", 0);

			Deposit deposit = new Deposit(pawel, 100);
			transactionManager.AddTransaction(deposit);

			//command has been added to queue, but not executed
			Assert.IsTrue(transactionManager.HasPendingTransactions);
			Assert.AreEqual(0, pawel.Balance);

			//execute command
			transactionManager.ProcessPendingTransaction();

			Assert.IsFalse(transactionManager.HasPendingTransactions);
			Assert.AreEqual(100, pawel.Balance);

			//add a withdrawal, apply it, and verify the balance changed.
			Withdraw withdrawal = new Withdraw(pawel, 50);
			transactionManager.AddTransaction(withdrawal);

			transactionManager.ProcessPendingTransaction();

			Assert.IsFalse(transactionManager.HasPendingTransactions);
			Assert.AreEqual(50, pawel.Balance);


		}

		[TestMethod]
		public void OverdraftRemainsInPendingTransactions()
		{
			TransactionManager
		}
	}
}
