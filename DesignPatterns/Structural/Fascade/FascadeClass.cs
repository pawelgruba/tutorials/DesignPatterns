﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Fascade
{
	/*
	 *  Używany do przysłaniania czegoś, np do tworzenia wrapperów jak wrappery dla delphi pisane w C#
	 *  Pozwala zasłonić pewne części kodu tak aby nie zaśmiecały i nie kusiły :)
	 *  Wg wiki:
	 *  Fasada – wzorzec projektowy należący do grupy wzorców strukturalnych. 
	 *  Służy do ujednolicenia dostępu do złożonego systemu poprzez wystawienie uproszczonego, 
	 *  uporządkowanego interfejsu programistycznego, który ułatwia jego użycie.
	 */
	public class EmailCreator : IEmailFluentInterface
	{
		private MailMessage _mailMessage = new MailMessage();

		private EmailCreator(string fromAddress)
		{
			_mailMessage.Sender = new MailAddress(fromAddress);
		}

		public static IEmailFluentInterface CreateEmailFrom(string fromAddress)
		{
			return new EmailCreator(fromAddress);
		}

		public IEmailFluentInterface From(string fromAddress)
		{
			return this;
		}

		public IEmailFluentInterface To(params string[] toAddresses)
		{
			foreach (var toAddress in toAddresses)
			{
				_mailMessage.To.Add(new MailAddress(toAddress));
			}

			return this;
		}

		public IEmailFluentInterface CC(params string[] ccAddresses)
		{
			foreach (var item in ccAddresses)
			{
				_mailMessage.CC.Add(new MailAddress(item));
			}

			return this;
		}

		public IEmailFluentInterface BCC(params string[] bccAddresses)
		{
			foreach (var item in bccAddresses)
			{
				_mailMessage.Bcc.Add(new MailAddress(item));
			}

			return this;
		}

		public IEmailFluentInterface WithSubject(string subject)
		{
			_mailMessage.Subject = subject;

			return this;
		}

		public IEmailFluentInterface WithBody(string body)
		{
			_mailMessage.Body = body;

			return this;
		}

		public void Send()
		{
			using (SmtpClient client = new SmtpClient("mailserver"))
			{
				client.Credentials = CredentialCache.DefaultNetworkCredentials;
				client.Timeout = 500;

				client.Send(_mailMessage);
			}
		}

	}
}
