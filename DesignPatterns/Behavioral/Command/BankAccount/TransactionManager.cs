﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Command.BankAccount
{
	public class TransactionManager
	{
		private readonly List<ITransaction> _transaction = new List<ITransaction>();

		public bool HasPendingTransactions
		{
			get
			{
				return _transaction.Any(x => !x.IsCompleted);
			}
		}

		public void AddTransaction(ITransaction transaction)
		{
			_transaction.Add(transaction);
		}

		public void ProcessPendingTransaction()
		{
			foreach(ITransaction transaction in _transaction.Where(w => !w.IsCompleted))
			{
				transaction.Execute();
			}
		}
	}
}
