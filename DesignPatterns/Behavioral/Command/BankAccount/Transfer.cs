﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Command.BankAccount
{
	public class Transfer : ITransaction
	{
		private readonly decimal _amount;
		private readonly Account _fromAccount;
		private readonly Account _toAccount;

		public bool IsCompleted { get; set; }

		public Transfer(Account from, Account to, decimal amount)
		{
			_fromAccount = from;
			_toAccount = to;
			_amount = amount;
		}

		public void Execute()
		{
			if (_fromAccount.Balance > _amount)
			{
				_fromAccount.Balance -= _amount;
				_toAccount.Balance += _amount;

				IsCompleted = true;
			}
		}
	}
}
