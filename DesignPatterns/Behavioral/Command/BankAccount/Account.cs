﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Command.BankAccount
{
	public class Account
	{
		public string OwnerName { get; set; }
		public decimal Balance { get; set; }

		public Account(string owner, decimal balance)
		{
			OwnerName = owner;
			Balance = balance;
		}
	}
}
