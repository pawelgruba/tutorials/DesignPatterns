﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Memento
{
	public class CompleteMemento : IMementoComplex<CompleteMemento>
	{
		private readonly List<CompleteMemento> _mementos = new List<CompleteMemento>();

		public int Id { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }

		public CompleteMemento(int id, string name, string address, string city, string postalCode)
		{
			Id = id;
			Name = name;
			Address = address;
			City = city;
			PostalCode = postalCode;

			SaveMemento();
		}

		public void RevertToLast()
		{
			throw new NotImplementedException();
		}

		public void RevertToOriginal()
		{
			var firstMemento = _mementos.FirstOrDefault();

			if (firstMemento!= null)
			{
				SetValuesFromMementos(firstMemento);

				if (_mementos.Count > 1)
					_mementos.RemoveRange(1, _mementos.Count - 1);
			}
		}

		public void SaveMemento()
		{
			_mementos.Add(new CompleteMemento(Id, Name, Address, City, PostalCode));
		}


		public void SetValuesFromMementos(CompleteMemento input)
		{
			Id = input.Id;
			City = input.City;
			Address = input.Address;
			PostalCode = input.PostalCode;
		}
	}
}
