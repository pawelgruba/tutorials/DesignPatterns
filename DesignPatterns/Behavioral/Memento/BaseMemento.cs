﻿using System;
using System.Collections.Generic;

namespace DesignPatterns.Behavioral.Memento
{
	public class BaseMemento<T>
	{
		private List<KeyValuePair<int,T>> mementos;

		public int CurrentRevision
		{
			get
			{
				return mementos.Count;
			}
		}

	
	}
}