﻿namespace DesignPatterns.Behavioral.Memento
{
	public interface IMemento
	{
		void Revert();
	}
}