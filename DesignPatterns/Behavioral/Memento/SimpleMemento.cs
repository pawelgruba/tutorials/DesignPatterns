﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Memento
{

	public class SimpleMemento
	{
		private readonly string _originalName;
		private readonly string _originalAddress;
		private readonly string _originalCity;
		private readonly string _originalPostalCode;

		public int Id { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }

		public bool IsDirty
		{
			get
			{
				return Name != _originalName ||
					Address != _originalAddress ||
					City != _originalCity ||
					PostalCode != _originalPostalCode;
			}
		}

		public SimpleMemento(int id, string name, string address, string city, string postalCode)
		{
			Id = id;
			Name = name;
			Address = address;
			City = city;
			PostalCode = postalCode;

			_originalName = name;
			_originalAddress = address;
			_originalCity = city;
			_originalPostalCode = postalCode;
		}

		public void Revert()
		{
			Name = _originalPostalCode;
			Address = _originalAddress;
			City = _originalCity;
			PostalCode = _originalPostalCode;
		}

		
	}
}
