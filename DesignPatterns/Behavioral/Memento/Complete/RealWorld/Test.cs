﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Memento.Complete.RealWorld
{
	public class Test
	{
		public void RunTest()
		{
			SalesProspect salesProspect = new SalesProspect();
			salesProspect.Name = "Pawel";
			salesProspect.Phone = "555555555555";
			salesProspect.Budget = 500.0;

			ProspectMemory memory = new ProspectMemory();
			memory.Memento = salesProspect.SaveMemento();

			salesProspect.Name = "Wazniak";
			salesProspect.Phone = "000000000000000";
			salesProspect.Budget = 99999999999.0;

			salesProspect.RestoreMemento(memory.Memento);
		}
	}
}
