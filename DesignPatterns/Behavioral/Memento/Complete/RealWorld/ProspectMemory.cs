﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Memento.Complete.RealWorld
{
	public class ProspectMemory
	{
		private SalesMemento _memento;

		// Property
		public SalesMemento Memento
		{
			set { _memento = value; }
			get { return _memento; }
		}
	}
}
