﻿namespace DesignPatterns.Behavioral.Memento
{
	public interface IMementoComplex<T>
	{
		void RevertToLast();
		void RevertToOriginal();
		void SaveMemento();
		void SetValuesFromMementos(T input);
		
	}
}