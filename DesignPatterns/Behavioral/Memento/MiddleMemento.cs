﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Memento
{
	//Quite the same as simple, difference is we have only one private property

	public class MiddleMemento : IMemento
	{
		private readonly CustomerMemento _customerMemento;

		public int Id { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }

		public bool IsDirty
		{
			get
			{
				return Name != _customerMemento.Name ||
					Address != _customerMemento.Address ||
					City != _customerMemento.City ||
					PostalCode != _customerMemento.PostalCode;
			}
		}

		public MiddleMemento(int id, string name, string address, string city, string postalCode)
		{
			Id = id;
			Name = name;
			Address = address;
			City = city;
			PostalCode = postalCode;

			_customerMemento = new CustomerMemento(id, name, address, city, postalCode);
		}

		public void Revert()
		{
			if (_customerMemento != null)
			{
				Name = _customerMemento.Name;
				Address = _customerMemento.Address;
				City = _customerMemento.City;
				PostalCode = _customerMemento.PostalCode;
			}
		}

		//This is one of the rare cases where we might declare more than one class in a file.
		//CustomerMemento class will never be used any place, other than in the Customer class.
		//So, you can make it a private class inside the one class where it's used.
		//Or, you could put it in its own file, and declare it an internal or public class.
		private class CustomerMemento
		{
			public int Id { get; set; }
			public string Name { get; set; }
			public string Address { get; set; }
			public string City { get; set; }
			public string PostalCode { get; set; }



			public CustomerMemento(int id, string name, string address, string city, string postalCode)
			{
				Id = id;
				Name = name;
				Address = address;
				City = city;
				PostalCode = postalCode;
			}
		}

	}
}
