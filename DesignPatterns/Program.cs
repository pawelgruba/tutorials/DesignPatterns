﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTest;
using DesignPatterns.Creation.Factory;
using DesignPatterns.Creation.Factory.CannonExample;
using DesignPatterns.Structural.Fascade;

namespace DesignPatterns
{
	public class Program
	{
		static void Main(string[] args)
		{
			#region Singleton
			//--normal ussage
			//Singleton fromStuden = Singleton.GetInstance();
			//fromStuden.Test();
			//Singleton fromEmployee = Singleton.GetInstance();
			//fromEmployee.Test();

			//--why private constructor?
			//var dupa = new Singleton(); - can't create instance like that due to private constructor

			//--why sealed is needed?
			//var cos = new SingletoneTemplate.DerivedSingleton(); - due to 'sealed' accossor has been added
			//cos.Test();

			//--thread safe
			Parallel.Invoke(
				PrintStudentsDetails,
				PrintEmployeeDetails
				);
			//PrintStudentsDetails();
			//PrintEmployeeDetails();



			#endregion

			#region Factory

			CannonFactory cannonCreator = CannonFactory.GetInstance();
			ICannon laserCannon = cannonCreator.Create<ILaserCannon>();
			laserCannon.Fire();
			laserCannon = cannonCreator.Create<IPlasmaCannon>();
			laserCannon.Fire();

			#endregion

			#region FASCADE
			//Structural.Fascade.EmailCreator.CreateEmailFrom("pawel").CC("dupa", "dwie").BCC("ja").WithSubject("temat").WithBody("body").Send();
			//EmailCreator.CreateEmailFrom("test").From("coś").Send();
			#endregion

			#region MEMENTO
			Behavioral.Memento.Complete.RealWorld.Test mementoRealWorldTest = new Behavioral.Memento.Complete.RealWorld.Test();
			mementoRealWorldTest.RunTest();
			#endregion

		}

		private static void PrintStudentsDetails()
		{
			Singleton fromStuden = Singleton.GetInstance();
			fromStuden.Test();
		}

		private static void PrintEmployeeDetails()
		{
			Singleton fromEmployee = Singleton.GetInstance();
			fromEmployee.Test();
		}
	}
}
