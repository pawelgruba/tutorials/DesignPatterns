﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creation.Factory.CannonExample
{
	public class CannonFactory
	{
		private CannonFactory()
		{
		}

		private static CannonFactory _instance = null;

		internal static CannonFactory GetInstance()
		{
			if (_instance == null)
				_instance = new CannonFactory();

			return _instance;
		}

		internal ICannon Create<T>()
		{
			Type cannonType = typeof(T);
			if (cannonType == typeof(IMachineCannon))
				return new MachineCannon();

			else if (cannonType == typeof(ILaserCannon))
				return new LaserCannon();

			else if (cannonType == typeof(IPlasmaCannon))
				return new PlasmaCannon();
			else return null;
		}
	}
}
