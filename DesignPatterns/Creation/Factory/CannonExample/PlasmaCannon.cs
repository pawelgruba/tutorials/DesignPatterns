﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creation.Factory.CannonExample
{
	public class PlasmaCannon : IPlasmaCannon
	{
		public void Fire()
		{
			Console.WriteLine("Fire using PlasmaCannon");
		}
	}
}
