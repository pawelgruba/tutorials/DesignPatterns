﻿namespace DesignPatterns.Creation.Factory.PlayerExample
{
	public class Player
	{
		public int HitPoints { get; private set; }
		public int ExperiencePoints { get; private set; }
		public int Gold { get; private set; }

		internal Player(int hitPoints, int experience, int gold)
		{
			this.HitPoints = HitPoints;
			this.ExperiencePoints = experience;
			this.Gold = gold;
		}
	}
}