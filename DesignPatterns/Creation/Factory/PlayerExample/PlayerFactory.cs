﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creation.Factory.PlayerExample
{
	public static class PlayerFactory
	{
		#region Initial
		public static Player LoadPlayer()
		{
			return new Player(10, 0, 10);
		}
#endregion
	}
}
