﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    /* nie może być bo SingletonTemplate ma prywatny konstruktor
    public class DerivedSingleton : SingletoneTemplate
    {
        
    }
     */
    public sealed class Singleton
    {
        private static Singleton _instance = null;
        private static int _instanceCounter;
        private static int _ussageCounter;
        private static readonly object Locker = new object();


        private Singleton()
        {
            _instanceCounter++;
        }

        public static Singleton GetInstance()
        {
            //double-check-locking mechanism
            if (_instance == null)
                lock (Locker)
                {
                    _ussageCounter++;

                    if (_instance == null)
                        _instance = new Singleton();
                }

            return _instance;

            //return _instance ?? (_instance = new Singleton());
        }

        public void Test()
        {
            Console.WriteLine(string.Format("'{0}' instances and '{1}' ussage", _instanceCounter, _ussageCounter));
        }

        /* sealed w definicji SingletoneTemplate zapobiega możliwości dziedziczenia po klasie SingletonTemplate
        public class DerivedSingleton : SingletoneTemplate
        {

        }
         */


    }
}

