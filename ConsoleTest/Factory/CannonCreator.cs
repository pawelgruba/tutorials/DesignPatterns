﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ConsoleTest.Factory
{
    public class CannonCreator
    {
        private CannonCreator()
        {
        }

        private static CannonCreator _instance = null;

        internal static CannonCreator GetInstance()
        {
            if (_instance==null)
                _instance = new CannonCreator();
        }
    }
}
