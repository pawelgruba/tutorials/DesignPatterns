﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    /* nie może być bo SingletonTemplate ma prywatny konstruktor
    public class DerivedSingleton : SingletoneTemplate
    {
        
    }
     */
    public sealed class SingletoneTemplate
    {
        private static SingletoneTemplate _instance = null;
        private static int _instanceCounter;
        private static int _ussageCounter;

        private SingletoneTemplate()
        {
            _instanceCounter++;
        }

        public static SingletoneTemplate GetInstance()
        {
            //if (_instance==null)
            //    _instance = new SingletoneTemplate();
            //return _instance;
            _ussageCounter++;
            return _instance ?? (_instance = new SingletoneTemplate());
        }

        public void Test()
        {
            Console.WriteLine(string.Format("'{0}' instances and '{1}' ussage",_instanceCounter,_ussageCounter));
        }

        /* sealed w definicji SingletoneTemplate zapobiega możliwości dziedziczenia po klasie SingletonTemplate
        public class DerivedSingleton : SingletoneTemplate
        {

        }
         */


    }
    }

