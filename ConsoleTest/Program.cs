﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTest.Factory;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SingletoneTemplate.GetInstance().Test();

            SingletoneTemplate.GetInstance().Test();

            var dupa = new SingletoneTemplate();


            //var cos = new SingletoneTemplate.DerivedSingleton();

            //cos.Test();


            //Fabryka
            ConsoleTest.Factory.CannonCreator cannonCreator = CannonCreator.GetInstance();
            IAbstractCannon laserCannon = cannonCreator.Create(ConsoleTest.Factory.CannonType.Laser_cannon);


        }
    }
}


